import 'package:flutter/material.dart';
import 'package:flutter_project/page/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    const primaryColor = Color(0xFF1B2610);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RZM Location',
      theme: ThemeData(
        primaryColor: primaryColor,
      ),
      home: const MyHomePage(),
    );
  }
}










