import 'package:flutter/material.dart';
import 'package:flutter_project/components/navBar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context){
    var size = MediaQuery.of(context).size;
    //var platform = Theme.of(context).platform;

    return Scaffold(
      appBar : addAppBar(context, "RZM Location"),
      body: Container(
        height: size.height,
        width: size.width,
        color: Colors.white,
        padding: const EdgeInsets.only(top: 0, left: 0),
        child : ListView(
          children : <Widget>[
            Container(
              color: Colors.red,
              child: addVideoHomePage(size.height, size.width, "compresseur.jpg")
            ),
            Container(
                child: addSecondPart(size.width)
            ),
            Container(
                child: addSocialMedia()
            ),
          ],
        ),
      ),
    );
  }
}

Widget addVideoHomePage(height,width, path){
  return Container(
      height: height,
      width: width,
      child: getImageFromAsset(path),
  );
}

getImageFromAsset(path){
  path = "assets/images/" + path;
  return Image.asset(
     path,
    fit: BoxFit.cover,
  );
}

Widget addSecondPart(width){
  return Container(
      padding: const EdgeInsets.only(top:20,bottom: 20),
      child: Row(
          children : <Widget>[
            Container(
              width: width/2,
              child : addVideoHomePage(400.0,width, "sableuse.jpg"),
            ),
            Container(
              width: width/2,
              padding: const EdgeInsets.only(top: 10),
              child : addDesc(),
            ),
            ],
      )

  );
}

Widget addDesc(){
  const title = "Louez en toute sérénité";
  const accroche = "À votre service quand vous en avez besoin";
  const desc = "Société récente et indépendante,nous sommes une entreprise de location de matériels et d’outillage située à Sainte-Livrade-sur-Lot. Nous mettons à la disposition des professionnels ainsi que des particuliers de divers secteurs d’activité tout un ensemble de matériel et engins proposé en location.";
  return Column(
      children:[
        Container (
          padding: const EdgeInsets.all(5),
          child : const Text(
            title,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Color(0xFFA8AD3F),
              fontSize: 15,

          ),
        ),
        ),
        Container (
        padding: const EdgeInsets.only(top: 5, bottom: 5),
        child : const Text(
          accroche,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            color: Color(0xFF222B3C),
            fontSize: 10,
          ),
        ),
        ),
        Container (
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child :const Text(
          desc,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            color: Color(0xFF222B3C),
            fontSize: 10,
          ),
        ),
        ),
      ],
    );
}

addSocialMedia(){
  return Container(
      padding: const EdgeInsets.only(top:30),
      color: Color(0xFF222B3C),
      child: Column(
        children : <Widget>[
         const Text (
              "SUIVEZ NOUS SUR LES RÉSEAUX SOCIAUX",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          Container(
            padding: const EdgeInsets.only(top: 40, bottom: 40),
            child : addSocialMediaIcons(),
              ),
            const Text (
              "@2023 by ML",
              style: TextStyle(
                color: Colors.white,
                fontSize: 10,
                fontWeight: FontWeight.bold,
              ),
            ),
        ],
      )

  );

}

addSocialMediaIcons(){
  return Row (
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        Icon(FontAwesomeIcons.facebook, color: Colors.white),
        SizedBox(width: 50),
        Icon(FontAwesomeIcons.instagram, color: Colors.white),
      ],
    );
}










