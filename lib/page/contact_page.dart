import 'package:flutter/material.dart';
import 'package:flutter_project/components/navBar.dart';

class ContactPage extends StatelessWidget{
  const ContactPage({super.key});

  @override
  @override
  Widget build(BuildContext context){
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar : addAppBar(context, "RZM Location"),
        body: Container(
            height: size.height,
            width: size.width,
            color: Color(0xFF222B3C),
            padding: const EdgeInsets.only(top: 40, left: 0),
            child: Column (
              children: [
                Text(
                    "NOUS CONTACTER",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                  ),
                ),
                addCoordonate(),
                Container(
                  width: size.width,
                  height: 272,
                  margin: const EdgeInsets.only(top: 40),
                  color: Color(0xFFA8AD3F),
                  child :
                    Text ("map à venir"),
                ),
                const Text (
                  "@2023 by ML",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
          )
      ),
    );
}


}

addCoordonate(){
  return Container(
    margin: const EdgeInsets.only(top : 50, left: 50, right: 50),
    padding: const EdgeInsets.only(top : 50, left: 20, bottom: 50, right: 20),
    color: Colors.white,
    child: Column(
      children : [
        Row (
          children : [
            Container (
              margin: EdgeInsets.only(top: 5, bottom: 10, left: 5, right: 15),
              padding : EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Color(0xAAA8AD3F),
                  borderRadius: BorderRadius.all(Radius.circular(20))

              ),
              child : Icon(Icons.pin_drop_rounded,  color: const Color(0xFFA8AD3F)),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("95, route d'Agen"),
                Text("47110 Sainte Livrade sur Lot"),
              ],
            ),
          ],
        ),
        Row (
          children : [
            Container (
              margin: EdgeInsets.only(top: 10, bottom: 10, left: 5, right: 15),
              padding : EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Color(0xAAA8AD3F),
                  borderRadius: BorderRadius.all(Radius.circular(20))

              ),
              child : Icon(Icons.phone,  color: const Color(0xFFA8AD3F)),
            ),
            Text("06 09 43 23 59")
          ],
        ),
        Row (
          children : [
            Container (
              margin: EdgeInsets.only(top: 10, bottom: 5, left: 5, right: 15),
              padding : EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Color(0xAAA8AD3F),
                  borderRadius: BorderRadius.all(Radius.circular(20))

              ),
              child : Icon(Icons.mail_rounded,  color: const Color(0xFFA8AD3F)),
            ),
            Text("stephane@rzm.fr")
          ],
        ),
      ],
    ),
  );
}



