import 'package:flutter/material.dart';
import 'package:flutter_project/components/navBar.dart';
import 'dart:convert';
import 'package:flutter/services.dart';

class ProductPage extends StatefulWidget {
  const ProductPage({super.key});
  @override
  _ProductPageState createState() => _ProductPageState();
}


class _ProductPageState extends State<ProductPage> {
  List _machines = [];

// Fetch content from the json file
  Future<void> readJson() async {
  final String response = await rootBundle.loadString('assets/files/machines.json');
  final data = await json.decode(response);
  setState(() {
  _machines = data["machines"];
  });
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    readJson();
    return Scaffold(
      appBar: addAppBar(context, "RZM Location"),
      body: Container (
        padding: const EdgeInsets.all(25),
        color: Color(0xFF222B3C),
        child: ListView.builder(
              itemCount: _machines.length,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return Text(
                    "NOS PRODUITS",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                    ),
                  );
                }
                index -= 1;
                return addCard(
                    _machines[index]["name"], _machines[index]["description"],
                    _machines[index]["imagePath"]);
              }
            ),
        ),
    );
  }
}

addCard (name, description, imagePath){
  return Card(
    margin: const EdgeInsets.all(10),
    child: ListTile(
      leading: addImage(700.0, 70.0, imagePath),
      title: Text(name),
      subtitle: Text(description),
    ),
  );
}

getImageFromAsset(path){
  path = "assets/images" + path;
  return Image.asset(
    path,
    fit: BoxFit.cover,
  );
}

Widget addImage(height,width, path){
  return Container(
    height: height,
    width: width,
    child: getImageFromAsset(path),
  );
}

