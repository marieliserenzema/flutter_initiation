import 'package:flutter/material.dart';
import 'package:flutter_project/page/contact_page.dart';
import 'package:flutter_project/page/home_page.dart';
import 'package:flutter_project/page/product_page.dart';

addAppBar (context, title){
  const blue = Color(0xFF222B3C);
  return AppBar (
    backgroundColor: blue,
    leading: addLeadingIcon(),
    title: addTitle(title),
    centerTitle: true,
    actions: <Widget>[
      PopupMenuButton(
          onSelected: (value){
            if (value == MenuItem.item1){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=> const MyHomePage(),));
            } else if (value == MenuItem.item2){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=> const ProductPage(),));
            }else if (value == MenuItem.item3){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=> const ContactPage(),));
            }
          },
          icon: const Icon(Icons.menu, color: Colors.white,),
          itemBuilder: (context) => [
            const PopupMenuItem(
                value: MenuItem.item1,
                child : Text ('Accueil')
            ),
            const PopupMenuItem(
                value: MenuItem.item2,
                child : Text ('Nos Produits')
            ),
            const PopupMenuItem(
                value: MenuItem.item3,
                child : Text ('Contact')
            ),
          ]),
    ],
  );
}



Widget addLeadingIcon(){
  return Container(
      height: 20.0,
      width: 20.0,
      padding: const EdgeInsets.only(left: 5, top: 5, bottom: 5),
      child: getImageFromAsset()

  );
}

getImageFromAsset(){
  return Image.asset(
    "assets/images/logo.png",
    fit: BoxFit.contain,
  );
}


enum MenuItem {
  item1,
  item2,
  item3
}


Text addTitle( String myText) {
  return Text (
    myText,
    style: const TextStyle(
      fontFamily: 'RaleWay',
      color: Colors.white,
      fontSize: 20,


    ),
  );
}

