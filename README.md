# Initiation à FLutter

Ce projet est un site vitrine développé avec Flutter, qui permet de présenter une entreprise, ses produits et ses services. 
Ce site vitrine a été conçu dans le but de m'initier à Flutter, en me permettant de découvrir les fonctionnalités de ce framework 
de développement mobile multiplateforme.

## Fonctionnalités

- Voir les produits disponibles à la location
- Retrouver les coordonées de l'entreprise

## Captures d'écrans 

![image](./pics/Capture_1.png)
![image](./pics/Capture_2.png)


## Pré-requis

- Flutter 2.0 ou supérieu
- Android SDK 21 ou supérieur
- iOS 10.0 ou supérieur


## Installation

1. Clonez le dépôt :`

```
git clone https://gitlab.com/marieliserenzema/flutter_initiation.git
```

2. Installez les dépendances :

```
cd flutter_initiation
flutter pub get
```

3. Exécutez l'application :

```
flutter run
```


